package com.stratio.ingestion.sink.jdbc;

class JDBCSinkException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    JDBCSinkException(String message) {
        super(message);
    }

    JDBCSinkException(Throwable cause) {
        super(cause);
    }
}
