package com.stratio.ingestion.sink.jdbc;

import com.google.common.base.Strings;
import org.apache.flume.Channel;
import org.apache.flume.Context;
import org.apache.flume.CounterGroup;
import org.apache.flume.Event;
import org.apache.flume.Transaction;
import org.apache.flume.conf.Configurable;
import org.apache.flume.instrumentation.SinkCounter;
import org.apache.flume.sink.AbstractSink;
import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class JDBCSink extends AbstractSink implements Configurable {

    private static final Logger log = LoggerFactory.getLogger(JDBCSink.class);

    private static final int DEFAULT_BATCH_SIZE = 20;
    private static final String CONF_DRIVER = "driver";
    private static final String CONF_CONNECTION_STRING = "connectionString";
    private static final String CONF_SQL_DIALECT = "sqlDialect";
    private static final String CONF_TABLE = "table";
    private static final String CONF_BATCH_SIZE = "batchSize";
    private static final String CONF_SQL = "sql";
    private static final String CONF_USER = "username";
    private static final String CONF_PASSWORD = "password";

    private Connection connection;
    private DSLContext create;
    private SinkCounter sinkCounter;
    private int batchSize;
    private QueryGenerator queryGenerator;
    private final CounterGroup counterGroup = new CounterGroup();

    public JDBCSink() {
        super();
    }

    @Override
    public void configure(Context context) {
        final String driver = context.getString(CONF_DRIVER);
        final String connectionString = context.getString(CONF_CONNECTION_STRING);

        this.batchSize = context.getInteger(CONF_BATCH_SIZE, DEFAULT_BATCH_SIZE);

        try {
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new JDBCSinkException(ex);
        }
        String username = context.getString(CONF_USER);
        String password = context.getString(CONF_PASSWORD);

        connection = null;
        try {
            if (Strings.isNullOrEmpty(username) || Strings.isNullOrEmpty(password)) { //Non authorized
                connection = DriverManager.getConnection(connectionString);
            } else { //Authorized
                //TODO: check all possible connections to remove this if
                if (CONF_SQL_DIALECT.equals("H2")) {
                    connection = DriverManager.getConnection(connectionString + ";USER=" + username + ";PASSWORD=" + password);
                } else {
                    connection = DriverManager.getConnection(connectionString, username, password);
                }
            }

        } catch (SQLException ex) {
            throw new JDBCSinkException(ex);
        }
        try {
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
            throw new JDBCSinkException(ex);
        }

        final ConnectionProvider connectionProvider = new DefaultConnectionProvider(connection);
        final SQLDialect sqlDialect = SQLDialect.valueOf(context.getString(CONF_SQL_DIALECT).toUpperCase(Locale.ENGLISH));

        create = DSL.using(connectionProvider, sqlDialect);

        final String sql = context.getString(CONF_SQL);
        if (sql == null) {
            this.queryGenerator = new MappingQueryGenerator(create, context.getString(CONF_TABLE));
            log.info("MappingQueryGenerator created");
        } else {
            this.queryGenerator = new TemplateQueryGenerator(sqlDialect, sql);
            log.info("TemplateQueryGenerator created");
        }

        this.sinkCounter = new SinkCounter(this.getName());
    }

    @Override
    public Status process() {
        log.info("Executing JDBCSink.process()...");
        Status status = Status.READY;
        Channel channel = getChannel();
        Transaction txn = channel.getTransaction();

        try {
            log.info("Begin transaction");
            txn.begin();
            int count;
            List<Event> eventList = new ArrayList<>();
            for (count = 0; count < batchSize; ++count) {
                Event event = channel.take();
                log.info("Event took");
                if (event == null) {
                    break;
                }
                eventList.add(event);
            }

            if (count <= 0) {
                sinkCounter.incrementBatchEmptyCount();
                counterGroup.incrementAndGet("channel.underflow");
                status = Status.BACKOFF;
                log.info("Status backoff");
            } else {
                if (count < batchSize) {
                    sinkCounter.incrementBatchUnderflowCount();
                    status = Status.BACKOFF;
                } else {
                    sinkCounter.incrementBatchCompleteCount();
                }

                final boolean success = this.queryGenerator.executeQuery(create, eventList);
                if (!success) {
                    throw new JDBCSinkException("Query failed");
                }
                log.info("Pre commit");
                connection.commit();

                sinkCounter.addToEventDrainAttemptCount(count);
            }
            txn.commit();
            sinkCounter.addToEventDrainSuccessCount(count);
            counterGroup.incrementAndGet("transaction.success");
        } catch (Throwable t) {
            log.error("Exception during process", t);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error("Exception on rollback", ex);
            } finally {
                txn.rollback();
                status = Status.BACKOFF;
                this.sinkCounter.incrementConnectionFailedCount();
                if (t instanceof Error) {
                    throw new JDBCSinkException(t);
                }
            }
        } finally {
            txn.close();
        }
        return status;
    }

    public Status BACKUP_process() {
        Status status = Status.BACKOFF;
        Transaction transaction = this.getChannel().getTransaction();
        try {
            transaction.begin();
            List<Event> eventList = this.takeEventsFromChannel(
                    this.getChannel(), this.batchSize);
            status = Status.READY;
            if (!eventList.isEmpty()) {
                if (eventList.size() == this.batchSize) {
                    this.sinkCounter.incrementBatchCompleteCount();
                } else {
                    this.sinkCounter.incrementBatchUnderflowCount();
                }

                final boolean success = this.queryGenerator.executeQuery(create, eventList);
                if (!success) {
                    throw new JDBCSinkException("Query failed");
                }
                connection.commit();

                this.sinkCounter.addToEventDrainSuccessCount(eventList.size());
            } else {
                this.sinkCounter.incrementBatchEmptyCount();
            }

            transaction.commit();
            status = Status.READY;
        } catch (Throwable t) {
            log.error("Exception during process", t);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                log.error("Exception on rollback", ex);
            } finally {
                transaction.rollback();
                status = Status.BACKOFF;
                this.sinkCounter.incrementConnectionFailedCount();
                if (t instanceof Error) {
                    throw new JDBCSinkException(t);
                }
            }
        } finally {
            transaction.close();
        }
        return status;
    }

    @Override
    public synchronized void start() {
        this.sinkCounter.start();
        super.start();
    }

    @Override
    public synchronized void stop() {
        this.sinkCounter.stop();
        super.stop();
    }

    private List<Event> takeEventsFromChannel(Channel channel, int eventsToTake) {
        List<Event> events = new ArrayList<>();
        for (int i = 0; i < eventsToTake; i++) {
            this.sinkCounter.incrementEventDrainAttemptCount();
            events.add(channel.take());
        }
        events.removeAll(Collections.singleton(null));
        return events;
    }
}
