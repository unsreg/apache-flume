package com.stratio.ingestion.source.rest.url;

import org.apache.flume.conf.Configurable;

import java.util.Map;

public interface UrlHandler extends Configurable {

    String buildUrl(Map<String, String> properties);

    void updateFilterParameters(String filterParameters);
}
