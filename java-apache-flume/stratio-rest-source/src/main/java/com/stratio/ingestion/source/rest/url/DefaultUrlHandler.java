package com.stratio.ingestion.source.rest.url;

import org.apache.flume.Context;

import java.util.Map;

public class DefaultUrlHandler implements UrlHandler {
    private static final String URL = "url";

    /**
     * Returns url property value
     */
    @Override
    public String buildUrl(Map<String, String> properties) {
        return properties.get(URL);
    }

    @Override
    public void updateFilterParameters(String filterParameters) {

    }

    @Override
    public void configure(Context context) {

    }
}
