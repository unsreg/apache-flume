package com.stratio.ingestion.source.rest.url.filter;

import com.stratio.ingestion.source.rest.url.filter.type.CheckpointType;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class FilterHandler {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    String filterField;
    CheckpointType filterType;
    Map<String, String> context;

    /**
     * Obtain the last checkpoint element
     *
     * @param context Context properties map
     * @return Checkpoint value
     */
    public abstract Map<String, String> getLastFilter(Map<String, String> context);

    /**
     * Update the checkpoint value
     *
     * @param checkpoint checkpoint value
     */
    public abstract void updateFilter(String checkpoint);

    public abstract void configure(Map<String, String> context);

    public static <T> T getInstance(String fieldName, Class<T> type, Object... parameters) {
        T instance = null;
        try {
            if (parameters != null) {

                instance = type
                        .cast(Class.forName(fieldName).newInstance());
            } else {
                instance = type
                        .cast(Class.forName(fieldName).getDeclaredConstructor(getClasses(parameters)).newInstance
                                (parameters));
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return instance;
    }

    private static Class<?>[] getClasses(Object[] parameters) {
        List<Class<?>> classes = new ArrayList<>();

        for (Object parameter : parameters) {
            classes.add(parameter.getClass());
        }
        return (Class<?>[]) classes.toArray();
    }
}
