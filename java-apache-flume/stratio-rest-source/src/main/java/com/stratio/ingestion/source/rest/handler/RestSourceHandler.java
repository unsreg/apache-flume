package com.stratio.ingestion.source.rest.handler;

import org.apache.flume.Event;
import org.apache.flume.conf.Configurable;

import java.util.List;
import java.util.Map;

public interface RestSourceHandler extends Configurable {

    List<Event> getEvents(String body, Map<String, String> headers);
}
