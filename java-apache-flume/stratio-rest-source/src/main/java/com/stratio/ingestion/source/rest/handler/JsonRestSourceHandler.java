package com.stratio.ingestion.source.rest.handler;

import com.google.common.base.Charsets;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.event.EventBuilder;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JsonRestSourceHandler implements RestSourceHandler {

    private static final Logger LOG = LoggerFactory.getLogger(JsonRestSourceHandler.class);
    protected static final String DEFAULT_JSON_PATH = "";
    protected static final String CONF_PATH = "jsonPath";
    private String path;
    private Gson gson;
    private Type listType = new TypeToken<List<Event>>() {
    }.getType();

    public JsonRestSourceHandler() {
        this.gson = new GsonBuilder().disableHtmlEscaping().create();
    }

    @Override
    public List<Event> getEvents(String body, Map<String, String> headers) {

        List<Event> events = new ArrayList<>(0);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(body);
            if (jsonNode.isObject()) {
                events.add(buildSingleEvent(headers, findValue(jsonNode, path)));
            }
            if (jsonNode.isArray()) {
                final Iterator<JsonNode> elements = jsonNode.getElements();
                JsonNode element;
                while (elements.hasNext()) {
                    element = elements.next();
                    events.add(buildSingleEvent(headers, findValue(element, path)));
                }
            }
        } catch (Exception e) {
            LOG.warn("An error ocurred while response parsing. Then content is not valid: " + body);
        }
        return events;
    }

    private JsonNode findValue(JsonNode jsonNode, String path) {
        JsonNode node = jsonNode;
        if (!DEFAULT_JSON_PATH.equals(path)) {
            node = jsonNode.findValue(path);
        }
        return node;
    }

    private Event buildSingleEvent(Map<String, String> headers, JsonNode jsonNode) {
        return EventBuilder.withBody(jsonNode.toString(), Charsets.UTF_8, headers);
    }

    @Override
    public void configure(Context context) {
        path = context.getString(CONF_PATH, DEFAULT_JSON_PATH);
    }
}
