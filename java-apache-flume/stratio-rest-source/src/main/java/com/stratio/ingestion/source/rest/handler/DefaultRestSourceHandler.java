package com.stratio.ingestion.source.rest.handler;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.event.EventBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DefaultRestSourceHandler implements RestSourceHandler {

    private Gson gson;
//    private final Type listType = new TypeToken<List<JSONEvent>>() {
//    }.getType();

    public DefaultRestSourceHandler() {
        this.gson = new GsonBuilder().disableHtmlEscaping().create();
    }

    @Override
    public List<Event> getEvents(String body, Map<String, String> headers) {
        List<Event> events = new ArrayList<>(0);
        events.add(EventBuilder.withBody(body, Charsets.UTF_8, headers));

        return events;
    }

    @Override
    public void configure(Context context) {

    }
}
