package com.stratio.ingestion.source.rest.handler;

import com.stratio.ingestion.source.rest.exception.RestSourceException;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.event.JSONEvent;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlumeStatsRestSourceHandler implements RestSourceHandler {

    @Override
    public void configure(Context context) {
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Event> getEvents(final String body, final Map<String, String> httpHeaders) {
        final List<Event> events = new ArrayList<Event>(0);
        try {
            ObjectMapper mapper = new ObjectMapper();
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) mapper.readValue(body, Map.class);

            for (Map.Entry<String, Object> entry : map.entrySet()) {
                final Map<String, String> headers = new HashMap<>();
                final String key = entry.getKey();
                final String name = key.substring(key.indexOf(".") + 1);
                headers.put("Name", name);
                for (Map.Entry<String, Object> componentInfo : ((Map<String, Object>) entry.getValue()).entrySet()) {
                    headers.put(componentInfo.getKey(), componentInfo.getValue().toString());
                }
                final Event event = new JSONEvent();
                event.setHeaders(headers);
                event.setBody("".getBytes());
                events.add(event);
            }
        } catch (IOException | ClassCastException ex) {
            throw new RestSourceException(ex);
        }

        return events;

    }

}
