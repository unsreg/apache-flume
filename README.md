# apache-flume
h2_db connection example
tcp://localhost:9092/file:c:/root/usr/prj/git_remote/gitlab.com/apache-flume/bin/data/jdbc_sink;AUTO_SERVER=TRUE;IFEXISTS=TRUE;MVCC=TRUE;LOCK_TIMEOUT=15000

agent_01 postman body test data:
[
    {
        "headers": {
            "timestamp": "434324343",
            "host": "random_host.example.com"
        },
        "body": "some data some data some data some data some data some data some data some data some data some data some data some data some data some data some data some data "
    },
    {
        "headers": {
            "namenode": "namenode.example.com",
            "datanode": "random_datanode.example.com"
        },
        "body": "some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 some data 2 "
    }
]